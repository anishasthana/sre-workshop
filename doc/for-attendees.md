# Open Data Hub Basic Tutorial

This tutorial is intended as part of an interactive in-person workshop and will show users how to use the Open Data Hub and Ceph to analyze data using machine learning algorithms.

## Prerequisites

The following has been provided for you as prerequisites for the workshop tutorial:

- A running OpenShift cluster.
- An installed Open Data Hub operator in the OpenShift cluster.
- A project space in OpenShift for deploying and managing Open Data Hub.

## Deploying Open Data Hub to Your OpenShift Project Space

In order to get started with the Open Data Hub, you need to deploy it into your project namespace.

1. Go to the OpenShift console login page provided on your handout.

2. Enter the Attendee ID and Password on your handout provided during the workshop to log into OpenShift.

3. After logging in, go to your list of projects by clicking on `Home -> Projects`.  Click on the project with your Attendee ID to open that project namespace. Ex. user250.  This will take you to the project status page.

4. Now you can deploy the Open Data Hub into your project.  On the left side menu, go to `Catalog -> Installed Operators`.  This will display a list of installed operators to deploy.  Find and click on `Open Data Hub Operator`.
 ![installed-operators](../images/installed-operators.png)

5. Once the Open Data Hub operator is selected, click on `Open Data Hub` in the top bar, and then the blue `Create Open Data Hub button`.

6. The Open Data Hub Custom Resource (CR for short) will be displayed. The CR is used to configure the deployment of the ODH that the operator will spin up. Make sure the CR matches what is shown in [odh-sre-cr.yml](../source/odh-sre-cr.yml). Click on `Create` to deploy the Open Data Hub components and wait until the deployment is complete.

7. It will take a while for all of the Open Data Hub components to start up.  To ensure all pods of the Open Data Hub are deployed successfully, click on `Workloads -> Pods` on the left menu. Wait until all pods should show `Ready` in the Readiness column. This should take about 5 minutes to complete.
![pods-ready](../images/pods-ready.png)

8. If you want to get an overview of the different pods, services, and routes that the Operator deploys, you can go to `Catalog -> Installed Operators`, and then click on `Open Data Hub`. Under `Open Data Hub`, you can find and click on `example-opendatahub` and view the individual bits and pieces of the deployment under the `resources` tab.

9. Now you can start using JupyterHub to run Kafka workloads.  To do so, first you need to find the link to your JupyterHub.  Click on `Networking -> Routes` in the left menu.  Then click on the link to JupyterHub.  This is where you will run the rest of the tutorial.
![routes](../images/routes.png)

## Streaming Data and Monitoring Infrastructure

1. The `source/notebooks` folder contains two notebooks called `producer.ipynb` and `consumer.ipynb`. Open both of them (start with the producer) and start running the cells in both of them. Leave the cells running.

    The producer notebook is simply writing to the Kafka cluster deployed by the ODH Operator on a sample topic, whereas the consumer is constantly reading all the messages that are written there.

2. Once you have started running the Kafka notebooks, go back to the OpenShift console. Under `Networking -> Routes`, find the routes named `Grafana` and `Prometheus`, open the URLs for both of them and login with your OpenShift credentials.

3. Once you are in Grafana, download `Spark Metrics.json` found under `source/dashboards/` to your local environment. Now, click on the `Home` button towards the top-left of the Grafana landing page.
![grafana-home](../images/grafana-home.png)

4. Finally, click on `Import Dashboard -> Upload.json File` and upload `Spark Metrics.json` to see how metrics changed over time as the notebooks ran.
![grafana-import](../images/grafana-import.png)

5. Follow the instructions to create the Kafka dashboard. The final result of the dashboard can be found under `/source/dashboards/Kafka.json`

## Argo Workflows

1. Argo provides a way for you to run a series of steps (known as Workflows) on a given schedule. This can be especially powerful for data engineers who need to perform regular transformations on data.

2. First, let's deploy Argo to the cluster. To do so, just edit the Open Data Hub custom resource and change `odh_deploy` from `false` to `true`.

3. You will notice two new pods come up for Argo after a few seconds.   

4. Run the following command while logged into the OpenShift namespace.

        oc login <OpenShift_console_url>
        oc project <your_project_namespace>
        oc create -f https://raw.githubusercontent.com/argoproj/argo/master/examples/hello-world.yaml -n <namespace> 

5. Find the Argo UI URL by clicking on `argo-ui` deployment and in the `Resources` tab in the right pane. You will find the URL in the `Routes` section.
![Argo route](../images/argo-route.png)

6. You will see a new argo workflow pop up in the argo ui, as well as some new pods in the OpenShift interface.
![Argo UI](../images/argo-ui.png)

7. Clicking on the workflow will let you see the workflow details. Clicking on a given step will show more details. Once a job is run successfully all the steps will show up as green.
![Argo Workflow details](../images/argo-workflow.png)

8. That's it!  Thank you for participating in this tutorial.  If you have questions or would like to contribute to the Open Data Hub project, you can find us at [https://opendatahub.io/community.html](https://opendatahub.io/community.html)
